import uproot
import sys, os, re, glob
import numpy as np

from numba import jit



# Some hard coded parameters
numSili = 8
numStripPerChip = 128
numAsic = 3
numStrip = numStripPerChip * numAsic

# Per invertire le coordinate, per ciascun asic, dei primi due piani
VA2TAfirst = True



def readPede(pedeFile):
    
    # Carico il file di pedestallo
    print(f"Pedestal file {pedeFile} loading...")
    tmpMat = np.loadtxt(pedeFile)
    print(f"Pedestal file {pedeFile} loaded!!!")
    
    # Mi assicuro che ci siano le linee che mi aspetto
    assert (tmpMat.shape[0] == numStrip * numSili)
    
    
    # Carico i due vettori da usare
    pede = tmpMat[:,0].reshape(numStrip, numSili)
    rms = tmpMat[:,1].reshape(numStrip, numSili)
    rmscn = tmpMat[:,2].reshape(numStrip, numSili)
    
    
    # Se sono minori di 1, imposto a 1 [Viene fatto in ana_strip.F]
    rms[rms < 1] = 1
    rmscn[rmscn < 1] = 1
    
    # Credo che ci sia la parte per invertire le strip del VA2TA 
    # in ana_strip.F
    
    if VA2TAfirst:
        # First 2 sili
        for i in range(numAsic):
            pede[i*numStripPerChip:(i+1)*numStripPerChip,:2]   = np.flip(pede[i*numStripPerChip:(i+1)*numStripPerChip,:2], axis = 0)
            rms[i*numStripPerChip:(i+1)*numStripPerChip,:2]    = np.flip(rms[i*numStripPerChip:(i+1)*numStripPerChip,:2], axis = 0)
            rmscn[i*numStripPerChip:(i+1)*numStripPerChip,:2]  = np.flip(rmscn[i*numStripPerChip:(i+1)*numStripPerChip,:2], axis = 0)
            
    return (pede, rms, rmscn)
