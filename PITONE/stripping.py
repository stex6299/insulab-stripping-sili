import uproot
import sys, os, re, glob
import numpy as np

from numba import jit


class siliconDetector:
    
    
    # Init passando il percorso del root file
    def __init__(self, rootFile) -> None:
        self.rootFile = rootFile
        print(f"New siliconDetector object created - File {rootFile}")
        
        self.filePath, self.fileName = os.path.split(rootFile)
        self.numRun = int(self.fileName.split("_")[0].replace("run",""))
        
        # Pedestal file
        self.pedeFile = os.path.join(self.filePath,f"run{self.numRun}_pede.dat" )
        
        
        # Some hard coded parameters
        self.numSili = 8
        self.numStripPerChip = 128
        self.numAsic = 3
        self.numStrip = self.numStripPerChip * self.numAsic
        
        
    def __str__(self) -> str:
        return(f"Root file: {self.rootFile}\nRun number: {self.numRun}\nPede file: {self.pedeFile}")
        
        
        
    def describeFile(self):
        with uproot.open(self.rootFile)["h1"] as f:
            f.show()
            
    def inspectSingletModule(self, n:int = 1 ):
        
        Ivfas_strip = f"Ivfas_strip{n}"
        Ivfas_data = f"Ivfas_data{n}"
        
        
        with uproot.open(self.rootFile)["h1"] as f:
            numStrip = f.arrays(library = "np")[Ivfas_strip]
            dataStrip = f.arrays(library = "np")[Ivfas_data]
            
        print(numStrip)
        print(dataStrip)
        
        return



    @jit(nopython=True) 
    def readPede(self):
        
        # Carico il file di pedestallo
        print(f"Pedestal file {self.pedeFile} loading...")
        tmpMat = np.loadtxt(self.pedeFile)
        print(f"Pedestal file {self.pedeFile} loaded!!!")
        
        # Mi assicuro che ci siano le linee che mi aspetto
        assert (tmpMat.shape[0] == self.numStrip * self.numSili)
        
        
        # Carico i due vettori da usare
        self.pede = tmpMat[:,0].reshape(self.numStrip, self.numSili)
        self.rms = tmpMat[:,1].reshape(self.numStrip, self.numSili)
        self.rmscn = tmpMat[:,2].reshape(self.numStrip, self.numSili)
        
        
        # Se sono minori di 1, imposto a 1 [Viene fatto in ana_strip.F]
        self.rms[self.rms < 1] = 1
        self.rmscn[self.rmscn < 1] = 1
        
        # Credo che ci sia la parte per invertire le strip del VA2TA 
        # in ana_strip.F
        
        if self.VA2TAfirst:
            # First 2 sili
            for i in range(self.numAsic):
                self.pede[i*self.numStripPerChip:(i+1)*self.numStripPerChip,:2]   = np.flip(self.pede[i*self.numStripPerChip:(i+1)*self.numStripPerChip,:2], axis = 0)
                self.rms[i*self.numStripPerChip:(i+1)*self.numStripPerChip,:2]    = np.flip(self.rms[i*self.numStripPerChip:(i+1)*self.numStripPerChip,:2], axis = 0)
                self.rmscn[i*self.numStripPerChip:(i+1)*self.numStripPerChip,:2]  = np.flip(self.rmscn[i*self.numStripPerChip:(i+1)*self.numStripPerChip,:2], axis = 0)

        
        
        # Scrivo allo stesso modo di Michela la prima parte    
            # for j in range(2):
            #     k = 0
            #     for ichip in range(3):
            #         for ichan in range(128, 0, -1):
            #             i = ichip*128 + ichan - 1
                        
            #             xpede = self.pede[i, j]
            #             xrms = self.rms[i, j]
            #             xrmscn = self.rmscn[i, j]
                        
            #             k+=1
            # self.pede[:,:2] = xpede
            # self.rms[:,:2] = xrms
            # self.rmscn[:,:2] = xrmscn
            
            return
                
        
    
    def loadRawFile(self):
        pass
    
    
    def computeCommonMode(self):
        pass