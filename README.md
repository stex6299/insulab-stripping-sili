# What's here
Scopo di questa repository è creare un pitone in grado di effettuare lo *stripping dei dati raw* dei silici del gruppo insulab.  
Tale procedura consiste nel determinare il numero di cluster e la posizione di impatto di una particella a partire dalla carica depositata nelle singole strip.


# Setup env
```
conda create --name stripping numpy pandas matplotlib scipy numba
conda install -c anaconda h5py
pip install uproot
```

# Files
- [stripping.py](./PITONE/stripping.py) oggetto per stripping e [test.ipynb](./PITONE/test.ipynb) testing notebook **CURRENT NOT SUPPORTED**
- [strippingFunctions.py.py](./PITONE/strippingFunctions.py.py) libreria di funzioni per stripping e [testFuncts.ipynb.ipynb](./PITONE/testFuncts.ipynb.ipynb) testing notebook 